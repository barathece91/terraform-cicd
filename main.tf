module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "qa-vpc"
  cidr = "10.0.0.0/25"


  azs = ["us-east-1a", "us-east-1b", "us-east-1c"]

  database_subnets = ["10.0.0.0/27", "10.0.0.32/27", "10.0.0.64/27"]

  # Feature flags
  manage_default_route_table     = true
  manage_default_network_acl     = true
  enable_dns_hostnames           = true
  enable_dns_support             = true
  enable_nat_gateway             = false
  single_nat_gateway             = false
  enable_vpn_gateway             = false
  create_database_subnet_group   = false

  default_route_table_tags       = { DefaultRouteTable = true }
  default_network_acl_name       = "default"
  default_network_acl_ingress    = []
  default_network_acl_egress     = []
  database_dedicated_network_acl = true

  manage_default_security_group  = true
 default_security_group_ingress = []
  default_security_group_egress  = []
}

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"
  version = "~> 4"

  name        = "db"
  description = ""
  vpc_id      = module.vpc.vpc_id

  ingress_with_cidr_blocks = [
    {
      from_port   = 5432
      to_port     = 5432
      protocol    = "tcp"
      description = "Postgres access from within VPC"

      // This will need to be expanded if we add clusters.
      cidr_blocks = "0.0.0.0/0"
    },
  ]
}


module "folder1" {
  2 attributes

  subs id 
  rg_name
}

module "folder2" {
    subs id 
  rg_name
}